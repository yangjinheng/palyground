package main

import "net/http"

func main() {
	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(999)
		w.Write([]byte("hello world"))
	}))
	http.ListenAndServe("0.0.0.0:80", http.DefaultServeMux)
}
